## UrlExpander
A Telegram bot made to unshort shortened links from services like bit.ly, grabify and others. Which can get annoying waiting 5 seconds while viewing some sort of ads, auto downloading files, your IP getting grabbed, etc...

## Setuping
Firstly, install dependencies:

```pip install -r requirements.txt```

Then, you will need the following data to proceed:
- bot token, you can get at [BotFather](https://t.me/botfather)
- api id, you can get yours at [Telegram Website](https://my.telegram.org)
- api hash, you can get yours at [Telegram Website](https://my.telegram.org)

Then put each in the respective parameter in the config.py file.
That's it! You're good to go.

## Final notes
Thank you for your interest. TerabitSec wishes you a very nice day.