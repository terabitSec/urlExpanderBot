from urlexpander import unshort
from datetime import date
from datetime import datetime

import json

hour = datetime.now()
today = date.today()

def expand(message: str) -> str:
    url = message.replace('/expander', '').strip()
    items = ""

    # requesting data to gather links
    api_request = unshort(url)
    hops = list(api_request['hops'].values())
    res = ""

    for i, hop in enumerate(hops, start=1):
        link = f'{i}. {hop}\n'
        url_original = f'{min(hops)}'
        items += link
        res = hop

    current_date = today.strftime("%d %B, %Y")
    current_time = hour.strftime("%H:%M:%S")

    data = f'{current_date} - {current_time}'

    log = {
        'date': data,
        'api_response': api_request,
        }

    with open('logs.txt', 'a') as f:
        json.dump(log, f)
        f.write('\n')
    f.close()

    return f'URL Original:\n{url}\n\nURL Final:\n{res}\n\nRedirecionamentos:\n{items}'