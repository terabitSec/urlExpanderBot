"""
This is a privacy oriented bot made to unshort
annoying links which display ads, open a lot of tabs,
and even mine data/cryptocurrency.

Last update: 01/08/2022(Bash)
Check, check, last update: 15/12/2024(Bash)

"Miss you, Marina."
"""
from pyrogram import Client, filters
from groupwatcher import watch
from datetime import date as dt1
from datetime import datetime
from util import expand

import threading
import urlexpander
import pathlib
import config
import os

# bot object declaration
app = Client('urlExpander',
            api_id=config.API_ID or os.environ["API_ID"],
            api_hash=config.API_HASH or os.environ["API_HASH"],
            bot_token=config.BOT_TOKEN or os.environ["BOT_TOKEN"],
            workers=64,
            ipv6=False,)

CURR_DIR = pathlib.Path().resolve()
ASSETS = CURR_DIR / 'assets' # To be used in the future

print(f'Bot is running at: {CURR_DIR}')

# indexing groups for greet message
groups = []

# reading groups.txt to send the greeting message if the group is not in
try:
    with open(f'{CURR_DIR}/groupsin.txt', 'r') as e:
        groups = [int(group.replace('\n', '').strip()) for group in e]
        print(f'The bot is running in {len(groups)} groups.')

except FileNotFoundError:
    # creating a plain text file for further use
    with open('groupsin.txt', 'w') as f:
        print('groupsin.txt file created in order to work properly')
        pass


# expander command
@app.on_message(filters.command('exp') & filters.private)
def main(client, message):
    url = str(message.text).replace('/exp', '').strip()
    exp = expand(url)

    # bot message output
    message.reply(exp, disable_web_page_preview=True)


# group greeting
@app.on_message(filters.group)
def greet(client, message):
    if int(message.chat.id) not in groups:
        chat = message.chat.id
        groups.append(str(chat))
        with open('groupsin.txt', 'a') as e:
            e.write(str(chat) + '\n')
        e.close()
        app.send_photo(message.chat.id, f'{CURR_DIR}/assets/img.jpg', caption='Obrigado por adicionar-me ao vosso grupo.\nApenas deixe-me fazer o meu trabalho. Analisarei todas as mensagens a seguir em busca de serviços encurtadores de link registrados no nosso banco de dados.\nResponda uma mensagem contendo um link marcando-me ou utilize o /expander para eu analisar o link sem consultar o nosso banco de dados.\n\nUm oferecimento\n@terabitSec')
    
    watch(client, message)


# start command
@app.on_message(filters.command('start'))
def start(client, message):
    message.reply(f'Olá, {message.from_user.first_name}!\n\nSeja bem-vindo ao urlExpander! Posso desencurtar links aqui também: use o comando /exp <link>')


if __name__ == "__main__":
    app.run()