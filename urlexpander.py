import dns.resolver, requests

dns.resolver.default_resolver=dns.resolver.Resolver(configure=False)
dns.resolver.default_resolver.nameservers=['9.9.9.9', '149.112.112.112']

headers = {"User-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0"}

def isValid(url):
    if url.startswith('http://'):
        url = url[7:]
        httpIncluded = True
    elif url.startswith('https://'):
        url = url[8:]
        httpIncluded = True
    else:
        httpIncluded = False

    firstSlash = url.find('/')
    if not firstSlash < 0:
        url = url[:firstSlash]

    try:
        dns.resolver.resolve(url)
        isDnsValid = True
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer) as error:
        isDnsValid = False
    
    return {'httpIncluded': httpIncluded, 'isDnsValid': isDnsValid}


def unshort(short):
    output = isValid(short)

    if not output['isDnsValid']:
        return {'unshorted': False, 'isDnsValid': False}

    if not output['httpIncluded']:
        short = 'http://' + short
    
    request = requests.get(short, stream=True, headers=headers)
    hops = [hops.url for hops in request.history]
    if hops:
        if hops[-1] != request.url:
            hops.append(request.url)
    else:
        hops.append(request.url)


    output = {'hops': {index: hops[index] for index in range(len(hops))}, 'isDnsValid': True}
    return output