"""
This add group support for the bot
"""

from pyrogram import Client, filters
from datetime import date as dt1
from datetime import datetime
from string import digits
from pathlib import Path
from util import expand

import re
import json

app = Client('urlExpander')

hour = datetime.now()
today = dt1.today()
directory = Path().resolve()

servfiles_location = directory / 'services'

services = []
file_list = []

for entry in servfiles_location.iterdir():
    file_list.append(entry.name)

for item in file_list:
    item_location = servfiles_location / item
    with open(item_location, 'r') as fl:
        for line in fl:
            line = line.lower()
            line = line.replace('\n', '').replace(' ', '')
            if line in services:
                continue
            else:
                services.append(line)
        fl.close()

print(f'Supported services: {len(services)}')


def watch(client, message):
    links = []
    for service in services:
        if service in message.text:
            # print(service, "yay")
            # pattern = f'http.*{service}/.*'
            # match = re.match(pattern, message.text)
            # if match:
            url = message.text #match.group(0)
            exp = expand(url)
            # auto-search output message
            message.reply(exp, disable_web_page_preview=True)
        else:
            continue


# on /expander group call
@app.on_message(filters.command('exp'))
def expander(client, message):
    url = message.text.replace('/exp','').replace(' ','')
    exp = expand(url)

    # group: output message
    message.reply(exp, disable_web_page_preview=True)